package com.example.trendingdocs.demo;

import com.example.trendingdocs.dao.DocumentDao;
import com.example.trendingdocs.dao.DocumentViewDao;
import com.example.trendingdocs.dao.UserDao;
import com.example.trendingdocs.database.TrendingDocumentsDatabase;
import com.example.trendingdocs.model.Document;
import com.example.trendingdocs.model.DocumentView;
import com.example.trendingdocs.model.User;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;

public class DemoData {

    private static final User user1 = new User("Filip", "Novák", "fnovak23@gmail.com");
    private static final User user2 = new User("Josef", "Strejček", "strejc@gmail.com");
    private static final User user3 = new User("Antonín", "Koudelka", "koudy.an@gmail.com");
    private static final User user4 = new User("Arnošt", "Nýdrle", "arny55@gmail.com");
    private static final User user5 = new User("Jan", "Exner", "exner89@gmail.com");
    private static final User user6 = new User("Ondřej", "Petera", "opeter123@gmail.com");
    private static final User user7 = new User("František", "Munzar", "fanda.munz@gmail.com");

    public static final User[] USERS = new User[] { user1, user2, user3, user4, user5, user6, user7 };

    public static final Document[] DOCUMENTS = new Document[] {
            new Document(user2, "doc1", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed malesuada turpis sed eros dapibus, eget hendrerit arcu accumsan. Quisque in feugiat leo. Cras felis eros, viverra non tellus consectetur, mollis cursus metus. Donec tempor, lectus sit amet dictum iaculis, tortor tellus elementum erat, vitae bibendum sem est at erat. In dictum ut ex eget bibendum. Ut tincidunt porttitor nisi. Etiam posuere magna sollicitudin nisi ullamcorper sodales. Sed in magna tempus, suscipit quam sit amet, scelerisque nisl. Integer in libero ac nibh tempus imperdiet. Ut sagittis, mi in mollis ornare, mauris diam pulvinar augue, sed blandit odio dui in nibh. Suspendisse molestie, nisi tincidunt eleifend fringilla, risus dui blandit nisl, nec pharetra lorem urna at erat. Aliquam erat volutpat. Nulla sit amet varius quam. Fusce a semper lorem. Pellentesque consectetur nunc id tempus gravida. Morbi nisl dolor, volutpat at tristique quis, gravida in velit."),
            new Document(user1, "doc2", "Mauris eros dolor, sollicitudin non ante et, tincidunt venenatis sem. Ut a leo libero. Nunc sed ligula elit. Aenean sagittis egestas nunc, eget convallis sapien. Quisque sed molestie augue. Phasellus eleifend augue risus, ut interdum magna maximus at. Aenean tincidunt suscipit augue quis mollis. Maecenas ultricies aliquam cursus. Nunc eget ligula sit amet urna fringilla accumsan nec eget nulla. Mauris accumsan molestie maximus. Pellentesque faucibus risus ac purus ullamcorper semper. Nulla facilisi. In hac habitasse platea dictumst. Pellentesque vitae leo felis."),
            new Document(user5, "doc3", "Vestibulum scelerisque leo quis mollis sollicitudin. Suspendisse libero mi, scelerisque vestibulum suscipit et, mattis id ante. Suspendisse tincidunt feugiat magna et sagittis. Nulla bibendum sapien eget turpis varius ultrices ut vel libero. Etiam in mollis quam. Curabitur et leo nisl. Nulla suscipit pretium odio, vitae venenatis nisl venenatis vel. Aenean bibendum egestas enim, eu semper dolor varius sed."),
            new Document(user7, "doc4", "Vivamus vel rhoncus dolor, sit amet cursus turpis. Nulla volutpat quam lorem, vel feugiat eros eleifend in. Fusce dui metus, malesuada sit amet tempus ut, dignissim vitae augue. Cras lectus est, molestie gravida laoreet quis, imperdiet id ligula. Sed venenatis enim tellus, eget condimentum diam lobortis sit amet. Sed eget justo tincidunt, ornare risus iaculis, eleifend quam. Pellentesque quis arcu nulla. Maecenas eu iaculis ex, a ullamcorper eros. Aliquam accumsan finibus ipsum, sit amet lobortis turpis dignissim ut. Nunc dignissim ante non dolor tempor consectetur."),
            new Document(user4, "doc5", "Nullam scelerisque fringilla lorem a dignissim. Curabitur vitae purus et nibh tincidunt placerat. In nisl neque, condimentum vel commodo aliquam, condimentum sagittis nisi. Aenean condimentum vehicula risus in mattis. Nam dolor arcu, fermentum vel justo ac, pulvinar fermentum enim. Proin eu pellentesque ante, et mattis elit. Nunc feugiat sagittis tempus. Quisque interdum gravida orci, ac lobortis massa vestibulum et. Maecenas tincidunt elementum convallis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nunc feugiat porta turpis et pulvinar. Maecenas mauris leo, condimentum non consequat in, ultricies ut lorem. Quisque enim risus, lacinia id neque ac, consequat maximus ex. Nulla rhoncus, metus a aliquam rutrum, ex elit semper ante, quis consectetur leo urna varius elit. Cras pharetra diam fermentum fermentum aliquet. Aenean eu eleifend arcu, vel suscipit lectus."),
            new Document(user1, "doc6", "Nulla dictum urna ac erat laoreet, quis ornare neque blandit. Integer eros elit, ultricies non turpis non, mattis laoreet lorem. Nullam mollis ligula nec varius molestie. Etiam aliquam enim vitae mauris ullamcorper ultrices et sed mi. Proin malesuada fermentum nisi, volutpat porta leo dapibus feugiat. In vehicula dui nec est feugiat, id commodo lorem varius. Quisque eget ligula ultricies, interdum sapien vel, ullamcorper leo. Nulla vehicula odio mauris, ac mattis augue dapibus ac. Nunc et turpis elementum, volutpat lacus nec, pulvinar augue. Quisque eu imperdiet justo, interdum rhoncus libero. Duis lectus massa, ornare convallis ullamcorper pretium, scelerisque a sapien."),
            new Document(user1, "doc7", "Phasellus enim libero, luctus eu maximus non, tincidunt porttitor eros. Nulla interdum augue at sodales accumsan. In id quam velit. Aenean dapibus ipsum at bibendum interdum. Duis a ante luctus, congue lectus ac, fermentum ligula. Nunc ac justo vitae quam imperdiet commodo. Donec quis lacinia leo. Phasellus nisl elit, aliquam at aliquam sed, laoreet eu lacus. Cras et aliquam purus. Suspendisse potenti. Nunc efficitur, nisl a tincidunt vestibulum, est metus euismod lorem, et cursus ligula est ac tortor. Pellentesque feugiat tortor in turpis vehicula, ac sodales nisi lobortis. Quisque eu lacus risus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer sit amet venenatis tellus, ut aliquet magna."),
            new Document(user2, "doc8", "Phasellus sed augue eu leo suscipit sodales. Vestibulum nibh dolor, feugiat a vestibulum at, varius in quam. Morbi vel tortor orci. Quisque id erat dolor. Morbi pellentesque ipsum lacus, eget scelerisque mi commodo eu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Donec placerat at dui quis gravida. Donec pellentesque purus at rhoncus sagittis. Nullam sed tortor quam. Nullam sagittis ac dui in lobortis. Praesent et nisi eros. Nulla id orci quis mauris tempus dapibus ac eget lacus. Curabitur eleifend orci non suscipit interdum. Nunc quis ex eget libero elementum eleifend. Etiam consequat dignissim eros, vitae volutpat purus pulvinar quis."),
            new Document(user3, "doc9", "Duis sit amet leo in urna luctus laoreet id ornare mauris. Nullam porta, lectus nec lobortis tristique, felis metus fermentum purus, euismod feugiat justo dolor a purus. Sed a ante tempus, tristique magna vel, fringilla lacus. Aliquam blandit venenatis purus non pulvinar. Duis quis erat eget neque egestas posuere ac vel orci. Fusce viverra tempor erat eu ornare. Cras in sapien in enim mattis ultrices. Fusce tempor viverra lorem eget iaculis. Suspendisse potenti. Fusce quis rutrum nisi. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus."),
            new Document(user5, "doc10", "Donec ornare tortor sit amet ex malesuada cursus. Duis ullamcorper purus vitae lacus ultricies, sed gravida elit aliquam. Donec pharetra metus odio, ac scelerisque massa consequat et. Nam nec sollicitudin mauris. Aenean aliquet lobortis eros, vitae semper nibh tincidunt sed. Cras pharetra dui vitae lectus bibendum ornare. Duis pharetra gravida lacus quis lobortis. In mattis dui a varius luctus. Integer pulvinar ultrices dapibus. Nam euismod venenatis tellus."),
            new Document(user5, "doc11", "Maecenas cursus dignissim lorem, eu tempus turpis finibus quis. Nulla consequat sed leo quis efficitur. Fusce vitae porta est. Praesent ipsum lacus, condimentum ac sollicitudin vitae, molestie sit amet mauris. Integer tincidunt lectus nec tellus tempus, in congue lectus cursus. Nam fringilla felis quis erat malesuada pellentesque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer ac eros accumsan, varius tellus fermentum, ultricies neque. Nam magna urna, sodales faucibus libero at, elementum tincidunt sapien. Integer at tincidunt dui. Etiam consequat, nulla et molestie vehicula, augue neque pulvinar ligula, et varius nunc quam non diam. Aliquam ultricies tellus velit, a euismod felis accumsan sed. Aenean id tellus sapien. Aliquam dictum in neque sit amet rhoncus."),
            new Document(user6, "doc12", "Praesent eget dignissim tellus. Donec ultrices, lectus id dignissim tristique, velit turpis suscipit eros, auctor pretium neque urna id libero. Pellentesque non dui orci. Donec quis scelerisque quam, a sodales mi. Praesent sem tortor, facilisis eu efficitur nec, varius et metus. Nunc vulputate rhoncus lacus eget aliquam. Maecenas vel porttitor elit. Nam pharetra lobortis lacus sed ornare. Cras euismod id lectus ut cursus. Curabitur quam augue, venenatis sed iaculis id, efficitur vel risus. Nam sed massa justo. Duis rutrum at est luctus hendrerit. Nullam lobortis odio tellus, a placerat urna finibus vitae. Donec id neque ac sem vulputate scelerisque. Aliquam ut tincidunt arcu, vel tincidunt purus. Aliquam laoreet ex ut urna hendrerit gravida."),
            new Document(user7, "doc13", "Donec eu nulla accumsan, molestie elit vel, bibendum risus. Donec vitae convallis urna. Etiam cursus est tortor, a pellentesque mi aliquam at. Aliquam erat volutpat. Nullam nisi felis, sagittis vitae vulputate ut, faucibus at velit. Cras cursus orci tempor semper porttitor. Nullam at suscipit leo. Nam porta odio id pharetra gravida. Duis eget diam semper, scelerisque ante ac, maximus arcu. Morbi vitae massa ut metus malesuada vulputate ac non ligula."),
            new Document(user1, "doc14", "Vestibulum vehicula enim metus. Morbi suscipit cursus malesuada. Vestibulum maximus sem quis massa tempor, ac faucibus lorem vestibulum. Donec ultricies nisl eget massa accumsan, eu varius mauris porta. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Suspendisse ornare sapien arcu, in tempus ante porttitor sed. Donec egestas ex at odio rutrum, quis euismod velit molestie. Mauris metus dolor, egestas ut augue vel, luctus semper nisl. Aliquam sed sem nec ipsum venenatis semper eu ac sapien. Morbi vel enim tincidunt, blandit neque vitae, ullamcorper tortor. Aenean vel risus nisi. Etiam viverra risus in enim laoreet, ut suscipit nibh venenatis. Nunc a diam aliquet, vulputate tortor in, tristique dolor. Quisque a aliquet orci."),
            new Document(user2, "doc15", "Aenean ut orci quam. Nullam augue mi, consectetur eget dolor efficitur, fringilla accumsan mi. Duis at malesuada nibh. Suspendisse at sapien quis orci tempus posuere. Aenean sodales odio ac arcu consequat, non tempus tortor blandit. Curabitur sed ligula lacus. Aliquam erat volutpat. Quisque lacinia eget turpis in semper. Suspendisse sodales mattis dolor eu iaculis. Morbi quis purus nec orci egestas rhoncus ut vel nunc. Nunc nisl orci, imperdiet at lorem in, auctor imperdiet nisi. Aliquam erat volutpat. Aliquam ac nisl malesuada, bibendum risus ac, pharetra orci. Integer convallis ultrices nisi in congue. Nulla facilisi."),
            new Document(user4, "doc16", "Etiam in urna purus. Pellentesque facilisis et mauris in sodales. Nullam venenatis consequat pretium. Sed sem diam, rhoncus ac eros ac, fermentum posuere neque. Duis vehicula leo et diam varius, ut mollis turpis efficitur. Nunc fringilla augue ipsum, et varius nulla vulputate condimentum. Nullam blandit tortor at efficitur volutpat. Etiam finibus congue justo, at aliquet turpis imperdiet quis."),
            new Document(user6, "doc17", "Praesent eget nisi aliquet, varius nunc tempor, dignissim ligula. Vestibulum et nulla nunc. Duis pellentesque lectus finibus ligula gravida aliquet. Sed ultricies metus elementum, fermentum elit sed, molestie nisl. Nunc porttitor sit amet risus a euismod. Sed sit amet tortor mollis mauris lobortis mattis. Praesent mattis, sem hendrerit ornare posuere, massa mi maximus libero, vitae pharetra nisi odio vitae orci. Nunc purus sapien, volutpat nec nibh et, commodo dictum eros. Aliquam erat volutpat."),
            new Document(user2, "doc18", "Sed non lectus laoreet, vehicula dolor ac, condimentum tortor. Suspendisse vulputate metus ex, sit amet lacinia ipsum pellentesque ut. Etiam vel diam euismod, mattis mi dapibus, viverra velit. Cras bibendum porttitor mauris. Vivamus arcu purus, bibendum at venenatis id, tempor in tellus. Quisque bibendum faucibus nisl eget efficitur. Morbi ipsum velit, tempus eget erat in, volutpat viverra leo. Nullam at iaculis leo, nec sollicitudin mauris. Phasellus laoreet risus ac semper sollicitudin."),
            new Document(user1, "doc19", "Sed eget scelerisque sem, non congue nisi. Phasellus accumsan augue vitae eros hendrerit, rhoncus lacinia augue blandit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Maecenas sodales arcu a mauris placerat sodales. Aenean non eros ac est porta fermentum. Mauris eu cursus nibh. Nam pretium at lacus sit amet efficitur. Nulla auctor dignissim condimentum."),
            new Document(user6, "doc20", "Vivamus augue urna, vestibulum tempor venenatis non, sollicitudin id dolor. Donec scelerisque eu lectus vel scelerisque. Donec leo est, faucibus nec libero quis, rutrum convallis magna. Morbi elementum quam sit amet fermentum hendrerit. Maecenas ultrices, urna ac pretium blandit, justo arcu imperdiet urna, eu hendrerit est nisl sit amet augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Mauris blandit arcu et diam consequat fermentum. Etiam eros metus, imperdiet eu justo vel, venenatis maximus sapien. Donec sit amet nisl a eros dapibus commodo nec vitae odio.")
    };

    public static DocumentView[] generateViews(int viewsCount) {
        DocumentView[] views = new DocumentView[viewsCount];
        Random rnd = new Random();
        long timeOpened = System.currentTimeMillis();

        for (int i = 0; i < viewsCount; i++) {
            timeOpened = timeOpened - 500 - rnd.nextInt(3 * 60 * 60 * 1000);

            views[i] = new DocumentView(
                    DOCUMENTS[rnd.nextInt(DOCUMENTS.length)].getId(),
                    USERS[rnd.nextInt(USERS.length)].getId(),
                    timeOpened
            );
        }

        return views;
    }

    public static void prefillDatabaseWithDemoData() {
        try {
            createTables();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        UserDao userDao = new UserDao();
        for (User user : DemoData.USERS) {
            userDao.create(user);
        }

        DocumentDao documentDao = new DocumentDao();
        for (Document document : DemoData.DOCUMENTS) {
            documentDao.create(document);
        }

        DocumentView[] views = DemoData.generateViews(1500);
        DocumentViewDao documentViewDao = new DocumentViewDao();
        for (DocumentView view : views) {
            documentViewDao.create(view);
        }
    }

    private static void createTables() throws SQLException {
        TrendingDocumentsDatabase db = TrendingDocumentsDatabase.getInstance();
        Connection connection = db.connect();
        Statement statement = connection.createStatement();

        // document
        statement.execute("CREATE TABLE " + DocumentDao.TABLE_NAME +
                "(" + DocumentDao.COLUMN_ID + " VARCHAR(255) not NULL, " +
                DocumentDao.COLUMN_TITLE + " VARCHAR(255), " +
                DocumentDao.COLUMN_CONTENT + " TEXT, " +
                DocumentDao.COLUMN_OWNER_ID + " VARCHAR(255), " +
                " PRIMARY KEY ( " + DocumentDao.COLUMN_ID + " ))");
        // user
        statement.execute("CREATE TABLE " + UserDao.TABLE_NAME +
                "(" + UserDao.COLUMN_ID + " VARCHAR(255) not NULL, " +
                UserDao.COLUMN_NAME + " VARCHAR(255), " +
                UserDao.COLUMN_SURNAME + " VARCHAR(255), " +
                UserDao.COLUMN_EMAIL + " VARCHAR(255), " +
                " PRIMARY KEY ( " + UserDao.COLUMN_ID + " ))");
        // document view
        statement.execute("CREATE TABLE " + DocumentViewDao.TABLE_NAME +
                "(" + DocumentViewDao.COLUMN_ID + " VARCHAR(255) not NULL, " +
                DocumentViewDao.COLUMN_DOCUMENT_ID + " VARCHAR(255), " +
                DocumentViewDao.COLUMN_USER_ID + " VARCHAR(255), " +
                DocumentViewDao.COLUMN_TIME_OPENED + " BIGINT, " +
                " PRIMARY KEY ( " + DocumentViewDao.COLUMN_ID + " ))");

        connection.close();
    }
}
