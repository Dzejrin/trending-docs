package com.example.trendingdocs;

import com.example.trendingdocs.dao.DocumentViewDao;
import com.example.trendingdocs.dao.UserDao;
import com.example.trendingdocs.database.TrendingDocumentsDatabase;
import com.example.trendingdocs.demo.DemoData;
import com.example.trendingdocs.service.DocumentService;
import org.junit.Rule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.PostgreSQLContainer;

import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
class TrendingDocsApplicationTests {

	@Autowired
	private DocumentService documentService;
	@Autowired
	private UserDao userDao;
	@Autowired
	private DocumentViewDao documentViewDao;

	@Rule
	public PostgreSQLContainer postgres = new PostgreSQLContainer("postgres:11.1")
			.withDatabaseName("DOCUMENTS")
			.withUsername("sa")
			.withPassword("sa");

	@Test
	public void basicTest() {
		postgres.start();

		TrendingDocumentsDatabase.init(
				postgres.getJdbcUrl(),
				postgres.getUsername(),
				postgres.getPassword()
		);

		DemoData.prefillDatabaseWithDemoData();

		Assertions.assertEquals(
				DemoData.USERS.length,
				userDao.getAll().size(),
				"Amount of users should match demo data."
		);

		long now = System.currentTimeMillis();
		long weekAgo = now - 7 * 24 * 60 * 60 * 1000;
		Map<String, List<String>> trendingMap = documentService.getTrendingDocuments(weekAgo, now, 5);
		for (String key : trendingMap.keySet()) {
			Assertions.assertTrue(
					trendingMap.get(key).size() <= 5,
					"List (" + key + ") of trending docs exceeded the limit."
			);
		}

		int viewsCountBefore = documentViewDao.getAll().size();
		documentService.getDocument(documentService.getAllDocuments().get(0), "");
		int viewsCountAfter = documentViewDao.getAll().size();
		Assertions.assertEquals(
				viewsCountBefore + 1,
				viewsCountAfter,
				"Document view was not recorded."
		);

		postgres.stop();
	}
}
