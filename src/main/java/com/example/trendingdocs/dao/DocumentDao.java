package com.example.trendingdocs.dao;

import com.example.trendingdocs.model.Document;
import com.example.trendingdocs.utils.SQLExceptionUtils;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.*;

@Component("documentDao")
public class DocumentDao extends AbstractDao<Document> {

    public static final String TABLE_NAME = "DOCUMENT";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_OWNER_ID = "ownerId";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_CONTENT = "content";

    @Override
    public List<Document> getAll() {
        final List<Document> result = new ArrayList<>();
        final Connection connection = openConnection();
        final String sql = "SELECT * FROM " + TABLE_NAME;

        try {
            final Statement statement = connection.createStatement();
            final ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                final String id = resultSet.getString(COLUMN_ID);
                final String ownerId = resultSet.getString(COLUMN_OWNER_ID);
                final String title = resultSet.getString(COLUMN_TITLE);
                final String content = resultSet.getString(COLUMN_CONTENT);

                result.add(new Document(id, ownerId, title, content));
            }
        } catch (SQLException e) {
            SQLExceptionUtils.printFailedStatementDetails(sql, e);
        } finally {
            closeConnection(connection);
        }


        return result;
    }

    @Override
    public void create(Document document) {
        final Connection connection = openConnection();
        final String sql = "INSERT INTO " + TABLE_NAME + " VALUES (?, ?, ?, ?)";

        try {
            final PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, document.getId().toString());
            statement.setString(2, document.getTitle());
            statement.setString(3, document.getContent());
            statement.setString(4, document.getOwnerId().toString());

            statement.executeUpdate();
        } catch (SQLException e) {
            SQLExceptionUtils.printFailedStatementDetails(sql, e);
        } finally {
            closeConnection(connection);
        }
    }

    public Document getById(UUID id) {
        final Connection connection = openConnection();
        final String sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + COLUMN_ID + "=?";

        try {
            final PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, id.toString());
            final ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                final String ownerId = resultSet.getString(COLUMN_OWNER_ID);
                final String title = resultSet.getString(COLUMN_TITLE);
                final String content = resultSet.getString(COLUMN_CONTENT);

                return new Document(id.toString(), ownerId, title, content);
            }
        } catch (SQLException e) {
            SQLExceptionUtils.printFailedStatementDetails(sql, e);
        } finally {
            closeConnection(connection);
        }

        return null;
    }
}
