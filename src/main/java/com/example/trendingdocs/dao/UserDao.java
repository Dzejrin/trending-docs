package com.example.trendingdocs.dao;

import com.example.trendingdocs.model.User;
import com.example.trendingdocs.utils.SQLExceptionUtils;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.*;

@Component("userDao")
public class UserDao extends AbstractDao<User> {

    public static final String TABLE_NAME = "APP_USER";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_SURNAME = "surname";
    public static final String COLUMN_EMAIL = "email";

    @Override
    public List<User> getAll() {
        final List<User> result = new ArrayList<>();
        final Connection connection = openConnection();
        final String sql = "SELECT * FROM " + TABLE_NAME;

        try {
            final Statement statement = connection.createStatement();
            final ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                final String id = resultSet.getString(COLUMN_ID);
                final String name = resultSet.getString(COLUMN_NAME);
                final String surname = resultSet.getString(COLUMN_SURNAME);
                final String email = resultSet.getString(COLUMN_EMAIL);

                result.add(new User(id, name, surname, email));
            }
        } catch (SQLException e) {
            SQLExceptionUtils.printFailedStatementDetails(sql, e);
        } finally {
            closeConnection(connection);
        }

        return result;
    }

    @Override
    public void create(User user) {
        final Connection connection = openConnection();
        final String sql = "INSERT INTO " + TABLE_NAME + " VALUES (?, ?, ?, ?)";

        try {
            final PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, user.getId().toString());
            statement.setString(2, user.getName());
            statement.setString(3, user.getSurname());
            statement.setString(4, user.getEmail());

            statement.executeUpdate();
        } catch (SQLException e) {
            SQLExceptionUtils.printFailedStatementDetails(sql, e);
        } finally {
            closeConnection(connection);
        }
    }
}
