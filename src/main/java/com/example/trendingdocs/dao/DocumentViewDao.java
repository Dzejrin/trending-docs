package com.example.trendingdocs.dao;

import com.example.trendingdocs.model.DocumentView;
import com.example.trendingdocs.tasks.trending.DocumentStats;
import com.example.trendingdocs.utils.SQLExceptionUtils;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.*;

@Component("documentViewDao")
public class DocumentViewDao extends AbstractDao<DocumentView> {

    public static final String TABLE_NAME = "DOCUMENT_VIEW";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_DOCUMENT_ID = "documentId";
    public static final String COLUMN_USER_ID = "userId";
    public static final String COLUMN_TIME_OPENED = "timeOpened";

    private static final String ALIAS_VIEWS = "views";
    private static final String ALIAS_VIEWERS = "viewers";

    @Override
    public List<DocumentView> getAll() {
        final List<DocumentView> result = new ArrayList<>();
        final Connection connection = openConnection();
        final String sql = "SELECT * FROM " + TABLE_NAME;

        try {
            final Statement statement = connection.createStatement();
            final ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                final String id = resultSet.getString(COLUMN_ID);
                final String documentId = resultSet.getString(COLUMN_DOCUMENT_ID);
                final String userId = resultSet.getString(COLUMN_USER_ID);
                long timeOpened = resultSet.getLong(COLUMN_TIME_OPENED);

                result.add(new DocumentView(id, documentId, userId, timeOpened));
            }
        } catch (SQLException e) {
            SQLExceptionUtils.printFailedStatementDetails(sql, e);
        } finally {
            closeConnection(connection);
        }

        return result;
    }

    @Override
    public void create(DocumentView documentView) {
        final Connection connection = openConnection();
        final String userId = documentView.getUserId().isEmpty() ? null : documentView.getUserId();
        final String sql = "INSERT INTO " + TABLE_NAME + " VALUES (?, ?, ?, ?)";

        try {
            final PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, documentView.getId().toString());
            statement.setString(2, documentView.getDocumentId().toString());
            statement.setString(3, userId);
            statement.setLong(4, documentView.getTimeOpened());

            statement.executeUpdate();
        } catch (SQLException e) {
            SQLExceptionUtils.printFailedStatementDetails(sql, e);
        } finally {
            closeConnection(connection);
        }
    }

    public void create(UUID userId, UUID documentId) {
        create(
                new DocumentView(
                        documentId,
                        userId,
                        System.currentTimeMillis()
                )
        );
    }

    public void create(UUID documentId) {
        create(
                new DocumentView(
                        documentId,
                        null,
                        System.currentTimeMillis()
                )
        );
    }

    public List<DocumentStats> getDocumentStatsFromInterval(long fromTime, long toTime) {
        final List<DocumentStats> result = new ArrayList<>();
        final Connection connection = openConnection();
        final String sql = "SELECT " + COLUMN_DOCUMENT_ID + ","
                + " COUNT(" + COLUMN_DOCUMENT_ID + ") AS " + ALIAS_VIEWS + ","
                + " COUNT(DISTINCT " + COLUMN_USER_ID + ") AS " + ALIAS_VIEWERS
                + " FROM " + TABLE_NAME + " WHERE "
                + COLUMN_TIME_OPENED + ">=? AND "
                + COLUMN_TIME_OPENED + "<=?"
                + " GROUP BY " + COLUMN_DOCUMENT_ID;

        try {
            final PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, fromTime);
            statement.setLong(2, toTime);
            final ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                final String documentId = resultSet.getString(COLUMN_DOCUMENT_ID);
                final int views = resultSet.getInt(ALIAS_VIEWS);
                final int viewers = resultSet.getInt(ALIAS_VIEWERS);

                result.add(new DocumentStats(UUID.fromString(documentId), views, viewers));
            }
        } catch (SQLException e) {
            SQLExceptionUtils.printFailedStatementDetails(sql, e);
        } finally {
            closeConnection(connection);
        }

        return result;
    }
}
