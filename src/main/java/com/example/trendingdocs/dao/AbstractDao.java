package com.example.trendingdocs.dao;

import com.example.trendingdocs.database.TrendingDocumentsDatabase;
import com.example.trendingdocs.utils.SQLExceptionUtils;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

abstract class AbstractDao<T> {

    public abstract List<T> getAll();
    public abstract void create(T object);

    protected Connection openConnection() {
        final TrendingDocumentsDatabase db = TrendingDocumentsDatabase.getInstance();
        Connection connection = null;

        try {
            connection = db.connect();
        } catch (SQLException e) {
            SQLExceptionUtils.printDetails(e);
        }

        return connection;
    }

    protected void closeConnection(Connection connection) {
        try {
            if (!connection.isClosed()) {
                connection.close();
            } else {
                System.out.println("Trying to close already closed database connection!");
            }
        } catch (SQLException e) {
            SQLExceptionUtils.printDetails(e);
        }
    }
}
