package com.example.trendingdocs.model;

import org.apache.logging.log4j.util.Strings;
import org.springframework.lang.Nullable;

import java.util.UUID;

public class DocumentView {
    private final UUID id;
    private final UUID documentId;
    private final String userId;
    private final long timeOpened;

    public DocumentView(UUID documentId, @Nullable UUID userId, long timeOpened) {
        this.id = UUID.randomUUID();
        this.documentId = documentId;
        this.userId = userId != null ? userId.toString() : Strings.EMPTY;
        this.timeOpened = timeOpened;
    }

    public DocumentView(String id, String documentId, String userId, long timeOpened) {
        this.id = UUID.fromString(id);
        this.documentId = UUID.fromString(documentId);
        this.userId = userId != null ? userId : Strings.EMPTY;
        this.timeOpened = timeOpened;
    }

    public UUID getId() {
        return id;
    }

    public UUID getDocumentId() {
        return documentId;
    }

    public String getUserId() {
        return userId;
    }

    public long getTimeOpened() {
        return timeOpened;
    }
}
