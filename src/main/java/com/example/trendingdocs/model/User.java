package com.example.trendingdocs.model;

import java.util.UUID;

public class User {
    private final UUID id;
    private final String name;
    private final String surname;
    private final String email;

    public User(String name, String surname, String email) {
        id = UUID.randomUUID();
        this.name = name;
        this.surname = surname;
        this.email = email;
    }

    public User(String id, String name, String surname, String email) {
        this.id = UUID.fromString(id);
        this.name = name;
        this.surname = surname;
        this.email = email;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }
}
