package com.example.trendingdocs.model;

import java.util.UUID;

public class Document {
    private final UUID id;
    private final UUID ownerId;
    private final String title;
    private final String content;

    public Document(User owner, String title, String content) {
        id = UUID.randomUUID();
        this.ownerId = owner.getId();
        this.title = title;
        this.content = content;
    }

    public Document(String id, String ownerId, String title, String content) {
        this.id = UUID.fromString(id);
        this.ownerId = UUID.fromString(ownerId);
        this.title = title;
        this.content = content;
    }

    public UUID getId() {
        return id;
    }

    public UUID getOwnerId() {
        return ownerId;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }
}
