package com.example.trendingdocs.tasks.trending;

import java.util.UUID;

public class TrendingDocumentStats {
    private final UUID documentId;
    private final int viewsProgression;
    private final int viewersProgression;

    TrendingDocumentStats(UUID documentId, int viewsProgression, int viewersProgression) {
        this.documentId = documentId;
        this.viewsProgression = viewsProgression;
        this.viewersProgression = viewersProgression;
    }

    public UUID getDocumentId() {
        return documentId;
    }

    public Integer getViewsProgression() {
        return viewsProgression;
    }

    public Integer getViewersProgression() {
        return viewersProgression;
    }
}
