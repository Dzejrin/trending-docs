package com.example.trendingdocs.tasks.trending;

import com.example.trendingdocs.dao.DocumentViewDao;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component("trendingDocumentsManager")
public class TrendingDocumentsManager {

    private static final String KEY_MOST_VIEWS = "mostViews";
    private static final String KEY_MOST_VIEWERS = "mostViewers";
    private static final String KEY_TRENDING_BY_VIEWS = "trendingByViews";
    private static final String KEY_TRENDING_BY_VIEWERS = "trendingByViewers";

    public Map<String, List<String>> findTrendingDocuments(long fromTime, long toTime, int limit) {
        final long intervalLength = toTime - fromTime;
        final DocumentViewDao documentViewDao = new DocumentViewDao();

        final List<DocumentStats> statsInInterval = documentViewDao.getDocumentStatsFromInterval(fromTime, toTime);
        final List<DocumentStats> statsInPreviousInterval = documentViewDao.getDocumentStatsFromInterval(fromTime - intervalLength, fromTime);
        final List<TrendingDocumentStats> trendingStatsInInterval = findTrendingStatsInInterval(statsInInterval, statsInPreviousInterval);

        final List<UUID> documentsWithMostViews = findDocumentsWithMostViews(statsInInterval, limit);
        final List<UUID> documentsWithMostViewers = findDocumentsWithMostViewers(statsInInterval, limit);
        final List<UUID> documentsTrendingByViews = findTrendingByViews(trendingStatsInInterval, limit);
        final List<UUID> documentsTrendingByViewers = findTrendingByViewers(trendingStatsInInterval, limit);

        Map<String, List<String>> result = new HashMap<>();
        putDocumentsIntoResultMap(result, KEY_MOST_VIEWS, documentsWithMostViews);
        putDocumentsIntoResultMap(result, KEY_MOST_VIEWERS, documentsWithMostViewers);
        putDocumentsIntoResultMap(result, KEY_TRENDING_BY_VIEWS, documentsTrendingByViews);
        putDocumentsIntoResultMap(result, KEY_TRENDING_BY_VIEWERS, documentsTrendingByViewers);

        return result;
    }

    private List<TrendingDocumentStats> findTrendingStatsInInterval(List<DocumentStats> statsInInterval, List<DocumentStats> statsInPreviousInterval) {
        final List<TrendingDocumentStats> trendingDocumentStats = new ArrayList<>(statsInInterval.size());

        statsInInterval.forEach(currentDocumentStats -> {
            Optional<DocumentStats> optionalPreviousDocumentStats = statsInPreviousInterval.stream()
                    .filter(stats -> stats.getDocumentId().equals(currentDocumentStats.getDocumentId()))
                    .findFirst();

            final int viewsProgression;
            final int viewersProgression;

            if (optionalPreviousDocumentStats.isPresent()) {
                DocumentStats previousDocumentStats = optionalPreviousDocumentStats.get();
                viewsProgression = currentDocumentStats.getViews() - previousDocumentStats.getViews();
                viewersProgression = currentDocumentStats.getViewers() - previousDocumentStats.getViewers();
            } else {
                viewsProgression = currentDocumentStats.getViews();
                viewersProgression = currentDocumentStats.getViewers();
            }

            trendingDocumentStats.add(
                    new TrendingDocumentStats(
                            currentDocumentStats.getDocumentId(),
                            viewsProgression,
                            viewersProgression
                    )
            );
        });

        return trendingDocumentStats;
    }

    private List<UUID> findDocumentsWithMostViews(List<DocumentStats> statsInInterval, int limit) {
        return statsInInterval.stream()
                .sorted((stats1, stats2) -> Integer.compare(stats2.getViews(), stats1.getViews()))
                .limit(limit)
                .map(DocumentStats::getDocumentId)
                .collect(Collectors.toList());
    }

    private List<UUID> findDocumentsWithMostViewers(List<DocumentStats> statsInInterval, int limit) {
        return statsInInterval.stream()
                .sorted((stats1, stats2) -> Integer.compare(stats2.getViewers(), stats1.getViewers()))
                .limit(limit)
                .map(DocumentStats::getDocumentId)
                .collect(Collectors.toList());
    }

    private List<UUID> findTrendingByViews(List<TrendingDocumentStats> trendingStatsInInterval, int limit) {
        return trendingStatsInInterval.stream()
                .sorted((stats1, stats2) -> Integer.compare(stats2.getViewsProgression(), stats1.getViewersProgression()))
                .limit(limit)
                .map(TrendingDocumentStats::getDocumentId)
                .collect(Collectors.toList());
    }

    private List<UUID> findTrendingByViewers(List<TrendingDocumentStats> trendingStatsInInterval, int limit) {
        return trendingStatsInInterval.stream()
                .sorted((stats1, stats2) -> Integer.compare(stats2.getViewersProgression(), stats1.getViewersProgression()))
                .limit(limit)
                .map(TrendingDocumentStats::getDocumentId)
                .collect(Collectors.toList());
    }

    private void putDocumentsIntoResultMap(Map<String, List<String>> resultMap, String key, List<UUID> documents) {
        resultMap.put(
                key,
                documents.stream()
                        .map(UUID::toString)
                        .collect(Collectors.toList())
        );
    }
}
