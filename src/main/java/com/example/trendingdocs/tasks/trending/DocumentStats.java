package com.example.trendingdocs.tasks.trending;

import java.util.UUID;

public class DocumentStats {
    private final UUID documentId;
    private final int views;
    private final int viewers;

    public DocumentStats(UUID documentId, int views, int viewers) {
        this.documentId = documentId;
        this.viewers = viewers;
        this.views = views;
    }

    public UUID getDocumentId() {
        return documentId;
    }

    public int getViewers() {
        return viewers;
    }

    public Integer getViews() {
        return views;
    }
}
