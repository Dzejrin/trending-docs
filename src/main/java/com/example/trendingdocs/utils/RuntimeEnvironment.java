package com.example.trendingdocs.utils;

public class RuntimeEnvironment {

    public static boolean isRunningJUnitTest() {
        for (StackTraceElement element : Thread.currentThread().getStackTrace()) {
            if (element.getClassName().startsWith("org.junit.")) {
                return true;
            }
        }
        return false;
    }
}
