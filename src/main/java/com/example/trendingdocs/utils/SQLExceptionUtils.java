package com.example.trendingdocs.utils;

import java.sql.SQLException;

public class SQLExceptionUtils {

    private SQLExceptionUtils() {}

    public static void printDetails(SQLException e) {
        printDetails("Database operation failed.", e);
    }

    public static void printDetails(String message, SQLException e) {
        System.out.println(message);
        System.out.println("Message: " + e.getMessage());
        System.out.println("ErrorCode: " + e.getErrorCode());
        System.out.println("SQLState: " + e.getSQLState());

        e.printStackTrace();
    }

    public static void printFailedStatementDetails(String sql, SQLException e) {
        final String message = "SQL statement failed: " + sql;
        printDetails(message, e);
    }
}
