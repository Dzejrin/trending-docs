package com.example.trendingdocs.service;

import com.example.trendingdocs.dao.DocumentDao;
import com.example.trendingdocs.dao.DocumentViewDao;
import com.example.trendingdocs.model.Document;
import com.example.trendingdocs.tasks.trending.TrendingDocumentsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Service("documentService")
public class DocumentService {

    @Autowired
    private DocumentDao documentDao;
    @Autowired
    private DocumentViewDao documentViewDao;
    @Autowired
    private TrendingDocumentsManager trendingDocumentsManager;

    public List<String> getAllDocuments() {
        return documentDao.getAll()
                .stream()
                .map(document -> document.getId().toString())
                .collect(Collectors.toList());
    }

    public Document getDocument(String id, String viewerId) {
        UUID documentId = UUID.fromString(id);

        if (viewerId.equals("")) {
            documentViewDao.create(documentId);
        } else {
            UUID userId = UUID.fromString(viewerId);
            documentViewDao.create(userId, documentId);
        }

        return documentDao.getById(UUID.fromString(id));
    }

    public Map<String, List<String>> getTrendingDocuments(long fromTime, long toTime, int limit) {
        return trendingDocumentsManager.findTrendingDocuments(fromTime, toTime, limit);
    }
}
