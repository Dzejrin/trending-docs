package com.example.trendingdocs.database;

import com.example.trendingdocs.utils.RuntimeEnvironment;

import java.sql.*;

public class TrendingDocumentsDatabase {

    private static volatile TrendingDocumentsDatabase instance;

    private static final String DATABASE_NAME = "DOCUMENTS";

    private final String url;
    private final String user;
    private final String password;

    private TrendingDocumentsDatabase(String url, String user, String password) {
        this.url = url;
        this.user = user;
        this.password = password;
    }

    public static void init(String url, String user, String password) {
        instance = new TrendingDocumentsDatabase(url, user, password);
    }

    public static TrendingDocumentsDatabase getInstance() {
        if (instance == null && !RuntimeEnvironment.isRunningJUnitTest()) {
            throw new IllegalStateException("Database not initialized, call init first.");
        }

        return instance;
    }

    public Connection connect() throws SQLException {
        return DriverManager.getConnection(url + DATABASE_NAME, user, password);
    }
}
