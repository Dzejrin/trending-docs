package com.example.trendingdocs;

import com.example.trendingdocs.database.TrendingDocumentsDatabase;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrendingDocsApplication {

	public static void main(String[] args) {
		TrendingDocumentsDatabase.init("jdbc:postgresql://localhost:2345/", "sa", "sa");

		SpringApplication.run(TrendingDocsApplication.class, args);
	}
}
