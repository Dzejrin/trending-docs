package com.example.trendingdocs.controller;

import com.example.trendingdocs.model.Document;
import com.example.trendingdocs.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
public class DocumentController {

    private static final String PATH_PREFIX = "/documents";

    @Autowired
    private DocumentService documentService;

    /**
     * Fetch list of all documents represented by their IDs.
     */
    @GetMapping(PATH_PREFIX + "/list")
    public List<String> allDocuments() {
        return documentService.getAllDocuments();
    }

    /**
     * Fetch full document, find it by its ID. Also saves record into DOCUMENT_VIEW database table.
     * @param id UUID of the document
     * @param viewerId UUID of user who opens the document, optional param
     * @return contents of one document
     */
    @GetMapping(PATH_PREFIX + "/get")
    public Document document(
            @RequestParam(value = "id") String id,
            @RequestParam(value = "viewer-id", required = false, defaultValue = "") String viewerId
    ) {
        return documentService.getDocument(id, viewerId);
    }

    /**
     * Find trending documents in specified interval.
     * @param fromTime unix timestamp, start of interval
     * @param toTime unix timestamp, end of interval
     * @param limit max amount of documents in list
     * @return map of trending documents.
     */
    @GetMapping(PATH_PREFIX + "/trending")
    public Map<String, List<String>> trendingDocuments(
            @RequestParam(value = "from") long fromTime,
            @RequestParam(value = "to") long toTime,
            @RequestParam(value = "limit") int limit
    ) {
        return documentService.getTrendingDocuments(fromTime, toTime, limit);
    }
}
